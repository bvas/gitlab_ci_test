﻿#include <iostream>
#include <vector>
#include <chrono>
#include <string>
#include <fstream>
#include <algorithm>
using namespace std;

int preracunajPot(vector<pair<int, int>> intervali, int dolzinaPoti);

int main(int argc, char** argv)
{
	auto start = chrono::high_resolution_clock::now();


	//string imeDatoteke = argv[1];
	string imeDatoteke = "testni_primer5.txt";

	ifstream datoteka(imeDatoteke);
	int x, y;
	vector<pair<int, int>> intervali;

	int iteracija = 1;
	int dolzinaPoti, steviloCrpalk;
	while (datoteka >> x >> y) {

		if (iteracija == 1) {
			dolzinaPoti = x;
			steviloCrpalk = y;
		}

		if (iteracija > 1) {
			int min = x - y;
			int max = x + y;

			intervali.push_back(make_pair(min, max));

		}
		iteracija++;

	}

	sort(intervali.begin(), intervali.end());

	int obiskaneCrpalke = preracunajPot(intervali, dolzinaPoti);

	if (obiskaneCrpalke == -1) {
		cout << obiskaneCrpalke << endl;
	}

	if (obiskaneCrpalke != -1) {
		cout << steviloCrpalk - obiskaneCrpalke << endl;
	}

	auto stop = chrono::high_resolution_clock::now();
	auto duration = chrono::duration_cast<chrono::microseconds>(stop - start);

	std::cout << "Trajanje: " << duration.count() * 0.000001 << "s" << endl;

}

int preracunajPot(vector<pair<int, int>> intervali, int dolzinaPoti) {

	int trenutniDomet = 0;
	int a, b;
	int obiskaneCrpalke = 0;
	while (trenutniDomet < dolzinaPoti) {
		//int najvecjiDomet = 0;
		//int index = -1;

		if (trenutniDomet == 0) {

			a = intervali[0].first;
			b = intervali[0].second;

			obiskaneCrpalke++;
			trenutniDomet = b;

		}
		else {
			int najvecjiDomet = 0;
			int index = -1;
			
			for (int i = 0; i < intervali.size(); i++) {
				if (intervali[i].first <= b && intervali[i].first > a) {
					if (intervali[i].second > najvecjiDomet) {
						najvecjiDomet = intervali[i].second;
						index = i;
					}
				}
			}



			if (index == -1) {
				return -1;
			}

			a = intervali[index].first;
			b = intervali[index].second;
			obiskaneCrpalke++;
			trenutniDomet = b;
		}
	}

	return obiskaneCrpalke;
}

